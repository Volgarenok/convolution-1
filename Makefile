.PHONY: all
.PHONY: clean

all: convolution summarizing

CXXFLAGS	+= -std=c++14 -Wall -Wextra

convolution: method_1.o
	$(CXX) $^ -o $@

summarizing: method_2.o
	$(CXX) $^ -o $@

clean:
	rm -rf *.o convolution summarizing
