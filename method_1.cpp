#include <iostream>
#include <fstream>
#include "dspdefs.hpp"
#include "dspdata.hpp"

int main(void)
{
  using namespace dsp::data;
  constexpr auto Hm = dsp::matrix< H::rows, H::columns, const int >{H::data};
  constexpr auto Xm = dsp::matrix< X::rows, X::columns, const int >{X::data};

  int * y  = new int[Y::rows * Y::columns]();
  auto Ym = dsp::matrix< Y::rows, Y::columns, int >{y};

  for(auto i = 0u; i < Y::rows; i++)
  {
    for(auto j = 0u; j < Y::columns; j++)
    {
      for(auto k1 = 0u; k1 < X::rows; k1++)
      {
	for(auto k2 = 0u; k2 < X::columns; k2++)
	{
	  if((i - k1 < H::rows) && (j - k2 < H::columns))
	  {
	    Ym[i][j] += Xm[k1][k2] * Hm[i - k1][j - k2];
	  }
	}
      }
    }
  }
  auto output = std::ofstream("method_1_y.txt");
  output << Ym << std::endl;
  delete [] y;
  return 0;
}
