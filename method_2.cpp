#include <iostream>
#include <fstream>
#include "dspdefs.hpp"
#include "dspdata.hpp"

int main(void)
{
  using namespace dsp::data;
  constexpr auto Hm = dsp::matrix< H::rows, H::columns, const int >{H::data};
  constexpr auto Xm = dsp::matrix< X::rows, X::columns, const int >{X::data};

  int * y  = new int[Y::rows * Y::columns]();
  auto Ym = dsp::matrix< Y::rows, Y::columns, int >{y};
  auto output = std::ofstream("method_2_temp.txt");
  for(auto k1 = 0u; k1 < X::rows; k1++)
  {
    for(auto k2 = 0u; k2 < X::columns; k2++)
    {
      auto temp = new int[Y::rows * Y::columns]();      
      auto Tempm = dsp::matrix< Y::rows, Y::columns, int >{temp};
      for(auto m1 = 0u; m1 < H::rows; m1++)
      {
	for(auto m2 = 0u; m2 < H::columns; m2++)
	{
	  Tempm[m1 + k1][m2 + k2] = Hm[m1][m2] * Xm[k1][k2];
	}
      }
      Ym += Tempm;
      output << "Temp: (" << k1 << "," << k2 << ")" << std::endl;
      output << Tempm << std::endl;
      delete [] temp;
    }
  }
  output = std::ofstream("method_2_y.txt");
  output << Ym;
  delete [] y;
  return 0;
}
