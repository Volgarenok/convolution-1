#ifndef DSPDATA_HPP
#define DSPDATA_HPP
namespace dsp
{
  namespace data
  {
    namespace H
    {
      constexpr auto rows = 8u;
      constexpr auto columns = 8u;
      constexpr int data[rows * columns] = {6, 2, 3, 3, 3, 3, 2, 0,
					    0, 2, 1, 2, 1, -1, 0, 0,
					    5, 4, 3, 4, 3, 4, -1, 0,
					    0, 5, 5, 0, 4, 2, 1, 0,
					    5, 0, 3, 4, 5, 5, 0, 4,
					    0, -1, -2, -1, 2, 3, 5, 0,
					    5, 0, -1, 1, 2, 1, 0, 0,
					    0, 4, 5, 0, 5, 0, 0, 0};
    }
    namespace X
    {
      constexpr auto rows = 8u;
      constexpr auto columns = 8u;
      constexpr int data[rows * columns] = {4, 6, 7, 8, 6, 4, 0, 4,
					    0, 2, 2, 3, 3, 4, 3, 5,
					    1, 2, 3, 2, 4, 5, 6, 0,
					    0, 3, 3, 3, 6, 4, 5, 6,
					    1, 4, 2, 3, 4, 5, 7, 6,
					    3, 3, 3, 0, 3, 5, 5, 6,
					    0, 0, 0, 3, 0, 0, 5, 0,
					    4, 7, 0, 8, 0, 8, 0, 0};
    }
    namespace Y
    {
      constexpr auto rows = H::rows + X::rows - 1;
      constexpr auto columns = H::columns + X::columns - 1;
    }
  }
}
#endif
