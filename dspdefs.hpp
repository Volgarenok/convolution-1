#ifndef DSPDEFS_HPP
#define DSPDEFS_HPP
#include <ostream>
#include <iterator>
namespace dsp
{
  namespace details
  {
    template< size_t Columns, class Integral >
    struct row
    {
      enum
      {
	n1 = 1,
	n2 = Columns
      };
      Integral & operator[](const size_t column_num)
      {
	return data_[column_num];
      }
      const Integral & operator[](const size_t column_num) const
      {
	return data_[column_num];
      }
      Integral * data_;
    };
    template< size_t Columns, class Integral >
    std::ostream & operator<<(std::ostream & os, const row< Columns, Integral > rhs)
    {
      std::copy(rhs.data_, rhs.data_ + row< Columns, Integral >::n2, std::ostream_iterator< Integral >(os, " "));
      os << ";";
      return os;
    }
  }

  template< size_t Rows, size_t Columns, class Integral >
  struct matrix
  {
    enum
    {
      n1 = Rows,
      n2 = Columns
    };
    details::row< n2, Integral > row(const size_t row_num) const
    {
      return {data_ + n2 * row_num};
    }
    details::row< n2, Integral > operator[](const size_t row_num)
    {
      return {data_ + n2 * row_num};
    }
    const details::row< n2, Integral > operator[](const size_t row_num) const
    {
      return {data_ + n2 * row_num};
    }
    matrix< Rows, Columns, Integral > & operator+=(const matrix< Rows, Columns, Integral > & rhs)
    {
      for(auto i = 0u; i < Rows; ++i)
      {
	for(auto j = 0u; j < Columns; ++j)
	{
	  data_[i * Columns + j] += rhs.data_[i * Columns + j];
	}
      }
      return *this;
    }
    Integral * data_;
  };
  template< size_t Rows, size_t Columns, class Integral >
  std::ostream & operator<<(std::ostream & os, const matrix< Rows, Columns, Integral > rhs)
  {
    for(auto i = 0u; i < matrix< Rows, Columns, Integral >::n1; ++i)
    {
      os << rhs[i] << std::endl;
    }
    return os;
  }
}
#endif
